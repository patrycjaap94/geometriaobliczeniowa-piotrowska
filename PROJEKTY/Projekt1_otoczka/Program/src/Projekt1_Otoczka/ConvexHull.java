package Projekt1_Otoczka;

/**
 *
 * @author patrycja piotrowska
 */
public class ConvexHull {

    /**
     *
     * Sprawdza zakręt po miedzy trzema punktami
     *
     * @param p1 - Punkt pierwszy
     * @param p2 - Punkt drugi
     * @param p3 - Punkt trzeci
     * @return true - jesli zakret pomiędzy trzema punktami skręca w prawo.
     * false - w przeciwnym przypadku false.
     *
     */
    public static boolean turnRight(MyPoint p1, MyPoint p2, MyPoint p3) {
        if ((p2.x - p1.x) * (p3.y - p1.y) - (p2.y - p1.y) * (p3.x - p1.x) < 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param o - Punkt pierwszy
     * @param a - Punkt drugi
     * @param b - Punkt trzeci
     * @return Zwraca double. Jeśli zwrócona liczba jest większa od 0 to skręca
     * w prawo. Jeśli 0 to brak skrętu. Jeśli mniejsza od zera to skręca w lewo
     */
    public static double turnRightDouble(MyPoint o, MyPoint a, MyPoint b) {

        return (a.x - o.x) * (b.y - o.y) - (a.y - o.y) * (b.x - o.x);
    }
}
