package Projekt1_Otoczka;
import java.util.*;
/**
 *
 * @author  patrycja piotrowska
 */
public class MainClass {

    /**
     
     * Główna klasa programu. Inicjuje GUI
     */
    public static void main(String[] args) {
         GUI.main();
    }
    
}
