package Projekt1_Otoczka;

import java.awt.Point;

/**
 * @author patrycja piotrowska
 * Klasa reprezentująca punkt, który składa się z współrzędnych x oraz y
 */
public class MyPoint extends Point implements Comparable<MyPoint> {

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public int compareTo(MyPoint p) {
        if (x == p.x && y == p.y) {
            return 0;
        }
        if (x == p.x) {
            return y < p.y ? -1 : 1;
        } else {
            return x < p.x ? -1 : 1;
        }
    }

}
