package Projekt1_Otoczka;

import java.awt.Color;
import java.awt.Image;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.util.*;


/**
 *
 * @author patrycja piotrowska
 */
public class DrawPanel extends JPanel {

    private final Color color = Color.GREEN; //Nadaje kolor punktowi na panelu rysującym
    private Image image;
    private final int r = 10;  // Nadaje promień punktowi na panelu rysującym 

    @Override
    public void paint(Graphics g) {
        if (image != null) {
            g.drawImage(image, 0, 0, this);

        } else {
            image = createImage(getWidth(), getHeight());
        }

    }
     
     
    
    
    public void drawPoint(MyPoint p) {
        Graphics g = image.getGraphics();
        g.setColor(color);
        g.fillOval(p.x - r, p.y - r, 2 * r, 2 * r);
        repaint();
    }
    
    /**
     * rysuje punkt po przesunięciu. 
     * @param colorx - kolor punktu
     * @param p - punkt
     */

    public void drawPoint(MyPoint p, Color colorx) {
        Graphics g = image.getGraphics();
        g.setColor(colorx);
        g.fillOval(p.x - r, p.y - r, 2 * r, 2 * r);
        repaint();
    }

    public void drawPoints(TreeSet<MyPoint> points) {

        for (MyPoint pp : points) {
            drawPoint(pp);
        }
        repaint();
    }

    public void clear() {
        Graphics g = image.getGraphics();
        g.clearRect(0, 0, getWidth(), getHeight());
        repaint();
    }

   
    void drawConvex(LinkedList<MyPoint> list) {
        Graphics g = image.getGraphics();
        g.setColor(Color.MAGENTA); // nadaje kolor otoczce wypukłej
        Iterator<MyPoint> iter = list.iterator();
        MyPoint p1 = null;
        MyPoint p2 = null;
        if (iter.hasNext()) {
            p1 = iter.next();
        } else {
            return;
        }

        for (; iter.hasNext();) {
            p2 = iter.next();
            g.drawLine(p1.x, p1.y, p2.x, p2.y);
            if (iter.hasNext()) {
                p1 = p2;
            } else {
                break;
            }
        }
        if (p2 != null) {
            p1 = list.getFirst();
            g.drawLine(p1.x, p1.y, p2.x, p2.y);
        }
        repaint();
    }
}
