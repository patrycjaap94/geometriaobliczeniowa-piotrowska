
package projekt2_przecinanie;
import java.util.TreeSet;
import java.util.Iterator;
import java.util.ListIterator;


/**
 *
 * @author patrycja piotrowska
 */
public class TState extends TreeSet<MyEdge> {

    @Override
    
    public MyEdge first() {
        if (size() > 0) {
            return super.first();
        } else {
            return null;
        }
    }

    @Override
    public MyEdge last() {
        if (size() > 0) {
            return super.last();
        } else {
            return null;
        }
    }
    

    @Override
    public boolean add(MyEdge e) {
        MyPoint P1;
        P1 = e.getP1();
        MyPoint P2;
        P2 = e.getP2();
        double yL = MyEdge.get_pE_L();
        if (P1.y >= yL && P2.y <= yL) { 
            return super.add(e);
        } else {
            return false;
        }
    }

    
    MyEdge onLeft(MyEdge e) {
        Iterator<MyEdge> it = iterator();
        MyEdge left;
        if (it.hasNext()) {
            left = it.next();
        } else {
            return null; 
        }
        MyEdge edge;
        while (it.hasNext()) {
            edge = it.next();
            if (edge == e) {
                return left;
            } else {
                left = edge;
            }
        }
        return null;
    }

    /**
     * zwraca krawędź, która znajduje się po lewej 
     * @param e - krawędź
     * @return - sąsiad lub null gdy nic nie znajdzie
     * 
     */
    MyEdge onRight(MyEdge e) {
        Iterator<MyEdge> it = iterator(); // iterator krawędzi
        MyEdge edge;
        while (it.hasNext()) {
            edge = it.next();
            if (edge == e)
            {
                if (it.hasNext()) {
                    return it.next(); 
                } else {
                    return null;      
                }
            }
        }
        return null;                
    }

}
