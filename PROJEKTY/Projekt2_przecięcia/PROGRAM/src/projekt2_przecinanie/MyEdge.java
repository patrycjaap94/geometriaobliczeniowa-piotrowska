package projekt2_przecinanie;

import java.util.Comparator;
import java.util.TreeSet;

/**
 * Klasa reprezentuje odcinek
 *
 * @author patrycja piotrowska
 */
public class MyEdge implements Comparable<MyEdge> {

    private MyPoint P1, P2; //P1- poczatek odcinka P2- koniec odcinka
    private static final MyPoint pE = new MyPoint(0, 0); // aktualny punkt zdarzeń, przez który przechodzi prosta L   
    private int counting_cross = 0; // Przechowuje ilość przecięcia konkretnego odcinka

    TreeSet<MyPoint> cross_points = new TreeSet<>(); // Zbiór punktow pokazujących, w którym miejscu przeciety jest odcinek

    public static void set_pE(MyPoint new_pE) {
        pE.x = new_pE.x;
        pE.y = new_pE.y;

    }

    /**
     * Zwraca położenie prostej pE.y
     *
     * @return położenie prostej L
     */
    public static double get_pE_L() {
        return pE.y;
    }

    /**
     * Konstruktor - sprawdza kolejność punktów
     *
     * @param p1 - punkt pierwszy
     * @param p2 - punk drugi
     */
    public MyEdge(MyPoint p1, MyPoint p2) {
        orderPoints(p1, p2);
    }

    /**
     * Oblicza odległosć p od prostej
     *
     * @param p - punkt
     * @return odległość p od prostej
     */
    public double distance2(MyPoint p) {

        double a = P2.y - P1.y;
        double b = P2.x - P1.x;
        double dist = Math.abs(-a * p.x + b * p.y - b * P1.y + a * P1.x) / Math.sqrt(a * a + b * b);
        return dist;
    }

    /**
     *
     * Metoda ustawia punkty zgodnie ze wspołrzędnymi
     *
     * @param p1 - punkt pierwszy
     * @param p2 - punkt drugi
     */
    public void orderPoints(MyPoint p1, MyPoint p2) {

        if (p1.y == p2.y) {
            if (p1.x <= p2.x) {
                P1 = new MyPoint(p1.x, p1.y);
                P2 = new MyPoint(p2.x, p2.y);
            } else {
                P1 = new MyPoint(p2.x, p2.y);
                P2 = new MyPoint(p1.x, p1.y);
            }
        } else if (p1.y > p2.y) {
            P1 = new MyPoint(p1.x, p1.y);
            P2 = new MyPoint(p2.x, p2.y);
        } else {
            P1 = new MyPoint(p2.x, p2.y);
            P2 = new MyPoint(p1.x, p1.y);
        }
    }

    /**
     *
     * @return zwraca początek odcinka
     */
    MyPoint getP1() {
        return P1;
    }

    /**
     *
     * @return zwraca koniec odcinka odcinka
     */
    MyPoint getP2() {
        return P2;
    }

    /**
     * Zamienia obiekt krawedzi w reprezetacje ciagu znaków
     *
     * @return zwraca współrzędne punktów i współrzędne przecieć w postaci
     * łańcucha znaków
     */
    @Override
    public String toString() {
        return "(" + P1.x + ", " + P1.y + ") : (" + P2.x + ", " + P2.y + ")" + this.cross_points;
    }

    @Override
    /**
     * Porównywanie krawędzi
     *
     * @param e - krawędź
     * @return zwraca 0 jeśli krawędzie są takie same 
     * zwraca -1 w pierwszym przypadku 
     * zwraca 1 w drugim przypadku
     */
    public int compareTo(MyEdge e) {
        if (this == e || (P1.compareTo(e.getP1()) == 0 && P2.compareTo(e.getP2()) == 0)) {
            return 0;
        }
        double x1, x2;
        double yL = pE.y;
        // przecięcia miotły
        if (P1.y == P2.y) // prosta pozioma
        {
            x1 = P2.x;
        } else if (P1.x == P2.x) // prosta pionowa
        {
            x1 = P1.x;
        } else // prosta ukośna 
        {
            x1 = (yL - P1.y) * (P2.x - P1.x) / (P2.y - P1.y) + P1.x;
        }

        if (e.P1.y == e.P2.y) {
            x2 = e.P2.x;
        } else if (e.P1.x == e.P2.x) {
            x2 = e.P1.x;
        } else {
            x2 = (yL - e.P1.y) * (e.P2.x - e.P1.x) / (e.P2.y - e.P1.y) + e.P1.x;
        }

        if (Math.abs(x1 - x2) < 1e-10) {
            if (Math.abs(x1 - pE.x) < 1e-10 || x1 < pE.x) // Czy prosta jest na/za punktem zdarzen
            {
                x1 = ((yL - 1) - P1.y) * (P2.x - P1.x) / (P2.y - P1.y) + P1.x;
                x2 = ((yL - 1) - e.P1.y) * (e.P2.x - e.P1.x) / (e.P2.y - e.P1.y) + e.P1.x;
            } else // czy punkt jest przed punktem zdarzen
            {
                x1 = ((yL + 1) - P1.y) * (P2.x - P1.x) / (P2.y - P1.y) + P1.x;
                x2 = ((yL + 1) - e.P1.y) * (e.P2.x - e.P1.x) / (e.P2.y - e.P1.y) + e.P1.x;
            }
        }
        if (x1 < x2) {
            return -1;
        } else {
            return 1;
        }
    }

    /**
     *
     * @param e1 - pierwszy odcinek
     * @param e2 - drugi odcinek
     * @param p - punkt przcięcia
     * @param p1 - punkt
     * @return 1 - jesli sie przecinają 
     * 2 - leżą na sobie 
     * 0 - nie przecinają sie
     */
    public static int calculateCrossPoint(MyEdge e1, MyEdge e2, MyPoint p, MyPoint p1) {
        double x;

        x = e1.getP1().x < e1.getP2().x ? e1.getP1().x : e1.getP2().x;
        double y;
        y = e1.getP1().y < e1.getP2().y ? e1.getP1().y : e1.getP2().y;
        MyPoint e1LD = new MyPoint(x, y);

        x = e1.getP1().x > e1.getP2().x ? e1.getP1().x : e1.getP2().x;
        y = e1.getP1().y > e1.getP2().y ? e1.getP1().y : e1.getP2().y;
        MyPoint e1PG = new MyPoint(x, y);

        x = e2.getP1().x < e2.getP2().x ? e2.getP1().x : e2.getP2().x;
        y = e2.getP1().y < e2.getP2().y ? e1.getP1().y : e2.getP2().y;
        MyPoint e2LD = new MyPoint(x, y);

        x = e2.getP1().x > e2.getP2().x ? e2.getP1().x : e2.getP2().x;
        y = e2.getP1().y > e2.getP2().y ? e1.getP1().y : e2.getP2().y;
        MyPoint e2PG = new MyPoint(x, y);

   
        if (!(e1PG.x >= e2LD.x && e2PG.x >= e1LD.x && e1PG.y >= e2LD.y && e2PG.y >= e1LD.y)) {
            return 0;
        }

        MyPoint vP3P1 = new MyPoint(e2.getP1().x - e1.getP1().x, e2.getP1().y - e1.getP1().y); // wektor P3-P1
        MyPoint vP2P1 = new MyPoint(e1.getP2().x - e1.getP1().x, e1.getP2().y - e1.getP1().y); // wektor P2-P1
        MyPoint vP4P1 = new MyPoint(e2.getP2().x - e1.getP1().x, e2.getP2().y - e1.getP1().y); // wektor P4-P1

        double v1 = det(vP3P1, vP2P1);
        double v2 = det(vP4P1, vP2P1);

        MyPoint vP1P3 = new MyPoint(e1.getP1().x - e2.getP1().x, e1.getP1().y - e2.getP1().y); // wektor P1-P3
        MyPoint vP4P3 = new MyPoint(e2.getP2().x - e2.getP1().x, e2.getP2().y - e2.getP1().y); // wektor P4-P3
        MyPoint vP2P3 = new MyPoint(e1.getP2().x - e2.getP1().x, e1.getP2().y - e2.getP1().y); // wektor P2-P3

        double v3 = det(vP1P3, vP4P3);
        double v4 = det(vP2P3, vP4P3);

        if (v1 * v2 > 0 || v3 * v4 > 0) {
            return 0;    // nie przecinają sie
        }
        if (Math.abs(v1) < 1e-8 && Math.abs(v2) < 1e-8) {

            return 2;
        } else {
            if (e1.getP1().x == e1.getP2().x) 
            {
                x = e1.getP1().x;
                y = (e2.getP2().y - e2.getP1().y) * x / (e2.getP2().x - e2.getP1().x) + e2.getP1().y;
            } else if (e2.getP1().x == e2.getP2().x) {
                x = e2.getP1().x;
                y = (e1.getP2().y - e1.getP1().y) * x / (e1.getP2().x - e1.getP1().x) + e1.getP1().y;
            } else {
                double a1 = (e1.getP2().y - e1.getP1().y) / (e1.getP2().x - e1.getP1().x);
                double a2 = (e2.getP2().y - e2.getP1().y) / (e2.getP2().x - e2.getP1().x);
                double b1 = e1.getP1().y - e1.getP1().x * (e1.getP2().y - e1.getP1().y) / (e1.getP2().x - e1.getP1().x);
                double b2 = e2.getP1().y - e2.getP1().x * (e2.getP2().y - e2.getP1().y) / (e2.getP2().x - e2.getP1().x);
                x = ((b2 - b1) / (a1 - a2));
                y = (a1 * (b2 - b1) / (a1 - a2) + b1);
            }
            p.x = x;
            p.y = y;
            return 1;
        }
    }

    public static double det(MyPoint p1, MyPoint p2) {
        return p1.x * p2.y - p1.y * p2.x;
    }

    
     /**
     * Sprawdza czy odcinki są jednakowe
     * @param e - odcinek
     * @return true - jesli odcinki są jednakowe
     * false - jeśli jest inaczej
     * */
    
    public boolean isEqual(MyEdge e) {
        if (P1.equals(e.getP1()) && P2.equals(e.getP2())) {
            return true;
        } else {
            return false;
        }
    }

    private void addPrzeciecie() {
        counting_cross = this.cross_points.size();
    }

    private void resetPrzeciecia() {
        counting_cross = 0;
    }

    public int getPrzeciecia() { 
        return counting_cross;
    }

    public void addPunktPrzeciecia(MyPoint p) {

        this.cross_points.add(p);
        this.addPrzeciecie();
    }

    public void resetPunktyPrzeciecia() {

        this.cross_points.clear();
        this.resetPrzeciecia();

    }

    public TreeSet<MyPoint> getPunktyPrzeciecia() {

        return this.cross_points;

    }

}

 /**
     * Komparator, który porównuje odcinki
     * @param o1 - odcinek pierwszy
     * @param o2 - odcinek drugi
     * @return 0 - jeśli odcinki są jednakowe
     * następnie porównuje współrzędne x. Jeśli takie same to porównuje współrzędne y
     * */

class EdgeComparator implements Comparator<MyEdge> {

    @Override
    public int compare(MyEdge o1, MyEdge o2) {
        if (o1 == o2) {
            return 0;
        }
        int comp = o1.getP1().compareTo(o2.getP1());
        if (comp == 0) {
            return o1.getP2().compareTo(o2.getP2());
        } else {
            return comp;
        }
    }
}
