/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt2_przecinanie;

import java.awt.Point;
import java.util.Comparator;

/**
 * Klasa reprezentująca punkt o współrzędnych.
 *
 * @author patrycja piotrowska
 */
public class MyPoint implements Comparable<MyPoint>, Cloneable {

    double x, y;

    public MyPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

  
    public MyPoint(Point p) {
        x = p.x;
        y = p.y;
    }

    /**
     * Metoda reprezentuje współrzędne punktu jako ciąg znaków
     *
     * @return - zwraca współprzędne punktu jako łańcuch znaków
     */
    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    /**
     * Metoda porównuje dwa punkty
     *
     * @param o - Obiekt
     * @return - Zwraca true jeśli współrzędne punktów są takie same. W
     * przeciwnym wypadku zwraca false
     */
    @Override
    public boolean equals(Object o) {
        if (o == null) {            // Sprawdzenie czy punkt instnieje
            return false;
        }
        if (!(o instanceof MyPoint)) { // Sprawdza czy obiekt jest punktem
            return false;
        }

        return ((int)x == (int)((MyPoint) o).x && (int)y == (int)((MyPoint) o).y);

    }

    /**
     * Metoda porównuje dwa punkty
     *
     * @param p - Punkt do porównania
     * @return - Zwraca 0 jeśli współrzędne punktów są takie same, 1 - jeśli
     * współrzędne punktu p są mniejsze -1 - jeśli współrzędne punktu p są
     * większe
     */
    @Override
    public int compareTo(MyPoint p) {

        if (this.equals(p)) {
            return 0;
        }
        if (x == p.x && y == p.y) {

            return 0;
        }
        if (x == p.x) {
            if (y < p.y) {
                return -1;
            } else {
                return 1;
            }

        } else if (x < p.x) {
            return -1;
        } else {
            return 1;
        }
    }

    /**
     * Metoda tworzy kopię punktu
     *
     * @return - kopia punktu
     */
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
    
    
   class MyPointComparator implements Comparator<MyPoint> {

    @Override
    public int compare(MyPoint o1, MyPoint o2) {
        if (o1.equals(o2)) {
            System.out.print("Pocisiontko 🙂");
            return 0;
        }
        int comp = ((Double) o1.x).compareTo(o2.x);
        if (comp == 0) {
            return ((Double) o1.y).compareTo(o2.y);
        } else {
            return comp;
        }
    }
}
}

