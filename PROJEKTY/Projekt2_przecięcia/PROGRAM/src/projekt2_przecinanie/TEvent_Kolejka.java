
package projekt2_przecinanie;

import java.util.Iterator;
import java.util.TreeSet;

/**
 * Klasa reprezentujaca kolejkę zdarzeń. 
 * @author patrycja piotrowska
 */
public class TEvent_Kolejka {
    
      private final TreeSet<TEvent> q_events = new TreeSet<>();    // Zbiór zdarzeń 
    
     /**
     * Pobiera i usuwa pierwszy element ze zbioru
     * @return zdarzenie z listy. Null jeśli zbiór pusty
     */
      
    TEvent getEvent ()
    {
        return q_events.pollFirst(); 
    }
    
    /**
     * Pobiera liczbę elementów kolejki
     * @return liczba całkowita elementów kolejki
     */
    public int size()
    {
        return q_events.size();
    }
    
    /**
     * Dodaje zdarzenie
     * @param e - zdarzenie (przecięcie)
     */
    void addEvent(TEvent e)
    {
        Iterator<TEvent> it = q_events.iterator(); //iterator, który będzie przechodził po zdarzeniach
        TEvent event = null; // ustawia zdarzenie na pusty
        boolean isEvent = false; 
        
        while ( it.hasNext() && isEvent == false ) // czy zdarzenie nie istnieje
        { 
            event = it.next();
            if ( event.compareTo(e) == 0 )
                isEvent = true;
        }
        if (isEvent)
        {
            if ( !e.getPktPoczatek().isEmpty() )
            {
                MyEdge edge = e.getPktPoczatek().first(); 
                event.addPktPoczatek( edge ); // dodawanie krawedzi
            }
            if ( e.getPktPrzeciecie().size() != 0 )
            {
                event.addPktPrzeciecie( e.getPktPrzeciecie().first() );
                event.addPktPrzeciecie( e.getPktPrzeciecie().last() );
            }
            if ( e.getPktKoniec().size() != 0 )
            {
                event.addPktKoniec( e.getPktKoniec().first() );
            }
            
        }
        else 
            q_events.add(e); // dodawanie do kolejki
    }
    
    
}
