package projekt2_przecinanie;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.LinkedList;
import java.util.TreeSet;
import javax.swing.JPanel;

/**
 *
 * @author patrycja piotrowska
 */
public class DrawPanel extends JPanel {

    private Image image;

    @Override
    public void paint(Graphics g) {
        if (image != null) {
            g.drawImage(image, 0, 0, this);
        } else {
            image = createImage(getWidth(), getHeight());
        }
    }

    /**
     * Rysuje pojedynczy odcinek
     *
     * @param e - Odcinek
     * @param c - kolor
     */
    public void drawEdge(MyEdge e, Color c) {
        Graphics g = image.getGraphics();
        g.setColor(c); //Ustawia koolor krawędzi na MAGENTA
        g.drawLine((int) (e.getP1().x + 0.5), (int) (e.getP1().y + 0.5),(int) (e.getP2().x + 0.5), (int) (e.getP2().y + 0.5));//rysuje linię
        repaint();
    }

    /**
     * Usuwa wszystko z ekranu
     */
    public void clear() {
        Graphics g = image.getGraphics();
        g.clearRect(0, 0, image.getWidth(this), image.getHeight(this));
        repaint();
    }

    /**
     * Rysuje krawędzie
     *
     * @param edges - krawędzie
     */
    public void drawEdges(LinkedList<MyEdge> edges) {
        Graphics g = image.getGraphics();

        for (MyEdge e : edges) {
            g.setColor(Color.BLACK); //ustawia kolor krawędzi ba BLACK
            g.drawLine((int) (e.getP1().x + 0.5), (int) (e.getP1().y + 0.5),
                    (int) (e.getP2().x + 0.5), (int) (e.getP2().y + 0.5));
        }
        repaint();
    }

    /**
     * Rysuje punkty, które zaznaczają przecięcia
     *
     * @param p - punkt
     */
    public void drawPoint(MyPoint p) {
        final int r = 6; // ustawia promień wyświetlanych okręgów
        Graphics g = image.getGraphics();
        g.setColor(Color.blue); // Ustawia kolor punktów na blue
        g.drawOval((int) (p.x + 0.5) - r, (int) (p.y + 0.5) - r, 2 * r, 2 * r);
        repaint();
    }

    /**
     * Rysuje punkty, które zaznaczają przecięcia (eliminując powtórzenia)
     *
     * @param p - Zbiór punktów bez powtórzeń przecięć
     */
    void drawPoints(TreeSet<MyPoint> p) {
        for (MyPoint e : p) {
            this.drawPoint(e);
        }
        repaint();
    }
}
