package projekt2_przecinanie;

import java.util.TreeSet;

/**
 * Klasa reprezentuje zdarzenia. Implementuje interfejs Comparable
 *
 * @author patrycja piotrowska
 */
public class TEvent implements Comparable<TEvent> {

    private final MyPoint p;
    private final TreeSet<MyEdge> pkt_koniec;    // Zbiór z krawędziami, których punkt jest koncem
    private final TreeSet<MyEdge> pkt_poczatek; // Zbiór z krawędziami, których punkt jest początkiem
    private final TreeSet<MyEdge> pkt_przeciecie; // Zbiór krawędzi, którym punkt jest przecięciem

    TEvent(MyPoint p) {
        this.p = p;
        pkt_poczatek = new TreeSet<>(new EdgeComparator()); // porównuje współrzędne x. Jeśli takie same to porównuje współrzędne y
        pkt_przeciecie = new TreeSet<>(new EdgeComparator()); // porównuje współrzędne x. Jeśli takie same to porównuje współrzędne y
        pkt_koniec = new TreeSet<>(new EdgeComparator()); // porównuje współrzędne x. Jeśli takie same to porównuje współrzędne y
    }

    @Override
    public int compareTo(TEvent e) {
        MyPoint event_p = e.getPoint();
        if (p.x == event_p.x && p.y == event_p.y) {
            return 0;
        } else if (p.y == event_p.y) {
            return p.x > event_p.x ? 1 : -1;
        } else {
            return p.y < event_p.y ? 1 : -1;
        }
    }

    /**
     * Dodaje krawędź, której punkt jest koncem
     *
     * @param e - krawędź
     */
    void addPktKoniec(MyEdge e) {
        pkt_koniec.add(e);
    }

    /**
     *
     * Dodaje krawędź, której punkt jest początkiem
     *
     * @param e - krawędź
     */
    void addPktPoczatek(MyEdge e) {
        pkt_poczatek.add(e);
    }

    /**
     * Dodaje krawędź, której punkt jest przecięciem
     *
     * @param e - krawędź
     */
    void addPktPrzeciecie(MyEdge e) {
        e.addPunktPrzeciecia(this.p);
        pkt_przeciecie.add(e);
    }

    /**
     * Zwraca zbiór krawędzi, gdzie p to koniec
     *
     * @return zbiór krawędzi, dla których p jest końcem
     */
    TreeSet<MyEdge> getPktKoniec() {
        return pkt_koniec;
    }

    /**
     * Zwraca zbiór krawędzi, gdzie p to początek
     *
     * @return zbiór krawędzi, gdzie p to początek
     */
    TreeSet<MyEdge> getPktPoczatek() {
        return pkt_poczatek;
    }

    /**
     * Zwraca zbiór krawędzi, dla których p jest punktem przecięcia
     *
     * @return zbiór krawędzi, dla których p jest punktem przecięcia
     */
    TreeSet<MyEdge> getPktPrzeciecie() {
        return pkt_przeciecie;
    }

    /**
     * Zwraca punkt
     *
     * @return p - punkt
     */
    public MyPoint getPoint() {
        return p;
    }

}
