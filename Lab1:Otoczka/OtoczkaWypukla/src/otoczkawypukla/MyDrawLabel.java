/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otoczkawypukla;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import javax.swing.JLabel;

/**
 *
 * @author patrycja
 */
public class MyDrawLabel extends JLabel {
    private Point p= new Point(-10,-10);
    private int r= 4;
    private Color color= Color.red;
    
    public void setPoint(Point p){
        this.p= p;
    }
    public Point getPoint(){
        return p;
    }
    
    public void Paint(Graphics g){
        g.setColor(color);
        g.fillOval(p.x -r, p.y -r, 2 * r, 2 * r);
    }
    
    public MyDrawLabel (int r, Color color){
        this.r= r;
        this.color= color;
    }
    
}
