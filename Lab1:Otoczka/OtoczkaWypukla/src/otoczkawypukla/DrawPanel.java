/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package otoczkawypukla;
import java.awt.Color;
import java.awt.Image;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Point;
import java.util.*;
import javax.swing.JLayeredPane;
/**
 *
 * @author patrycja
 */
public class DrawPanel extends JPanel {
    private final Color color = Color.magenta;
    private Image image;
    private int r = 4;  
    private JLayeredPane layeredPane;
    private MyDrawLabel drawLabel;
    private MyDrawLabel backLabel;
    private boolean isDrawing = false;
    private Point orgin;
    private int rr= 4;
    
    
    
    private MyDrawLabel createMyDrawLabel(Color color, Point p){
        MyDrawLabel label = new MyDrawLabel(rr, color);
        label.setOpaque(true);
        label.setBackground(color);
        label.setForeground(Color.black);
        label.setBounds(p.x, p.y, this.getWidth(), this.getHeight());
        return label;
        
    }
    
    @Override
    public void paint (Graphics g) {
        if(image != null)
            g.drawImage(image, 0, 0, this);
        else 
            image = createImage(getWidth(), getHeight());
    }
    
    public void drawPoint (MyPoint p){
        Graphics g = image.getGraphics();
        g.setColor(color);
        g.fillOval(p.x - r, p.y -r, 2 * r, 2 * r);
        repaint();
    }
    
        public void drawPoint (MyPoint p, Color colorx){
        Graphics g = image.getGraphics();
        g.setColor(colorx);
        g.fillOval(p.x - r, p.y -r, 2 * r, 2 * r);
        repaint();
    }
    
    public void drawPoints(TreeSet<MyPoint> points){
        
     for (MyPoint pp : points) {
            drawPoint(pp);
        }
     repaint();
    }
    
    public void clear(){
        Graphics g= image.getGraphics();
        g.clearRect(0, 0, getWidth(), getHeight());
        repaint();
    }
    
    public void setR (int R){
        r= R;
    }
    
    void drawConvex(LinkedList<MyPoint> list){
        Graphics g = image.getGraphics();
        g.setColor(Color.blue);
        Iterator<MyPoint> iter = list.iterator();
        MyPoint p1= null;
        MyPoint p2= null;
        if (iter.hasNext())
            p1 = iter.next();
        else
            return;     
        
        for (; iter.hasNext();){
            p2= iter.next();
            g.drawLine(p1.x,  p1.y, p2.x,  p2.y);
            if (iter.hasNext()){
                p1= p2;
            }
            else
                break;
        }
        if (p2 != null){
            p1 = list.getFirst();
            g.drawLine(p1.x, p1.y, p2.x,  p2.y);
        }
        repaint();
    }
}
