/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package przecinanie_krawedzi;

import java.util.Iterator;
import java.util.TreeSet;

/**
 *
 * @author patrycja
 */
public class TEventQueue {

    private TreeSet<TEvent> q_events = new TreeSet<>();

    public TEvent getEvent() {
        return q_events.pollFirst();
    }

    public int size() {
        return q_events.size();
    }

    void addEvent(TEvent e) {
        Iterator<TEvent> it = q_events.iterator();
        TEvent event = null;
        boolean isEvent = false;
        while (it.hasNext() && isEvent == false) {
            event = it.next();
            if (event.compareTo(e) == 0) {
                isEvent = true;
            }
        }
        if (isEvent) {
            if (e.getStartPoints().size() != 0) {
                event.addStartPoint(e.getStartPoints().first());
            }

            if (e.getEndPoints().size() != 0) {
                event.addEndPoint(e.getEndPoints().first());
            }

            if (e.getCrossPoints().size() != 0) {
                event.addCrossPoint(e.getCrossPoints().first());
                event.addCrossPoint(e.getCrossPoints().last());
            }
        } else {
            q_events.add(e);
        }

    }

}
