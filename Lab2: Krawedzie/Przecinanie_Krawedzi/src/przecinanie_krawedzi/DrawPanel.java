/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package przecinanie_krawedzi;
import java.awt.Color;
import java.awt.Image;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Point;
import java.util.*;
import javax.swing.JLayeredPane;
/**
 *
 * @author patrycja
 */
public class DrawPanel extends JPanel {
    private final Color color = Color.red;
    private Image image;
    private int r = 4;  
    private JLayeredPane layeredPane;
    private MyDrawLabel drawLabel;
    private MyDrawLabel backLabel;
    private boolean isDrawing = false;
    private Point orgin;
    private int rr= 4;
    
    
    private MyDrawLabel createMyDrawLabel(Color color, MyPoint p){
        MyDrawLabel label = new MyDrawLabel(rr, color);
        label.setOpaque(true);
        label.setBackground(color);
        label.setForeground(Color.black);
        label.setBounds((int)p.x, (int)p.y, this.getWidth(), this.getHeight());
        return label;
        
    }
    
    @Override
    public void paint (Graphics g) {
        if(image != null)
            g.drawImage(image, 0, 0, this);
        else 
            image = createImage(getWidth(), getHeight());
    }
    
    public void drawPoint (MyPoint p){
        Graphics g = image.getGraphics();
        g.setColor(color);
        g.fillOval((int)p.x - r, (int)p.y -r, 2 * r, 2 * r);
        repaint();
    }
    
    public void clear(){
        Graphics g= image.getGraphics();
        g.clearRect(0, 0, getWidth(), getHeight());
        repaint();
    }
    
    public void setR (int R){
        r= R;
    }
    
    void drawLine(MyEdge o){
        Graphics g = image.getGraphics();
        g.setColor(Color.blue);

        g.drawLine((int)(o.P1.getX()), (int)(o.P1.getY()), (int)(o.P2.getX()), (int)(o.P2.getY()));
         
        repaint();
    }
}
