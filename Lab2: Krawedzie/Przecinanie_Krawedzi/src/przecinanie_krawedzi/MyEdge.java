/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package przecinanie_krawedzi;

import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.Serializable;
import java.util.Comparator;

/**
 *
 * @author patrycja
 */
public class MyEdge implements Comparable<MyEdge>, Serializable {

    MyPoint P1;
    MyPoint P2;
    Color color;
    String name;
    private static MyPoint Pe = new MyPoint(0, 0);

    public MyEdge(int xp, int yp, int xk, int yk) {

        if (xp <= xk && yp <= yk) {
            P1 = new MyPoint(xp, yp);
            P2 = new MyPoint(xk, yk);
        } else {
            P2 = new MyPoint(xp, yp);
            P1 = new MyPoint(xk, yk);
        }
    }

    public MyEdge(MyPoint p1, MyPoint p2, Color color) {
        this.color = color;
        makePoinstOrdered(p1, p2);
    }

    public static void set_pE(MyPoint new_pE) {
        Pe.x = new_pE.x;
        Pe.y = new_pE.y;
    }

    public static MyPoint get_pE() {
        return Pe;
    }

    public final void makePoinstOrdered(MyPoint p1, MyPoint p2) {
        if (p1.y == p2.y) {
            if (p1.x <= p2.x) {
                P1 = new MyPoint(p1.x, p1.y);
                P2 = new MyPoint(p2.x, p2.y);
            } else {
                P1 = new MyPoint(p2.x, p2.y);
                P2 = new MyPoint(p1.x, p1.y);
            }
        } else if (p1.y > p2.y) {
            P1 = new MyPoint(p1.x, p1.y);
            P2 = new MyPoint(p2.x, p2.y);
        } else {
            P1 = new MyPoint(p2.x, p2.y);
            P2 = new MyPoint(p1.x, p1.y);
        }
    }

    public static void setpE(Point p) {
        Pe.x = p.x;
        Pe.y = p.y;
    }

    public static double get_pE_L() {
        return Pe.y;
    }

    public void move(int dx, int dy) {
        P1.x += dx;
        P2.x += dx;
        P1.y += dy;
        P2.y += dy;

    }

    MyPoint getP1() {
        return P1;
    }

    MyPoint getP2() {
        return P2;
    }

    public String toString() {
        return "(" + P1.toString() + ") (" + P2.toString() + ")";
    }

    public static int EdgeCross(MyEdge o1, MyEdge o2, MyPoint p, MyPoint p1) {
        double x, y;

        x = o1.P1.x > o1.P2.x ? o1.P1.x : o1.P2.x;
        y = o1.P1.y > o1.P2.y ? o1.P1.y : o1.P2.y;

        MyPoint o1pg = new MyPoint(x, y);

        x = o1.P1.x < o1.P2.x ? o1.P1.x : o1.P2.x;
        y = o1.P1.y < o1.P2.y ? o1.P1.y : o1.P2.y;

        MyPoint o1ld = new MyPoint(x, y);

        x = o2.P1.x > o2.P2.x ? o2.P1.x : o2.P2.x;
        y = o2.P1.y > o2.P2.y ? o2.P1.y : o2.P2.y;

        MyPoint o2pg = new MyPoint(x, y);
        x = o2.P1.x < o2.P2.x ? o2.P1.x : o2.P2.x;
        y = o2.P1.y < o2.P2.y ? o2.P1.y : o2.P2.y;
        MyPoint o2ld = new MyPoint(x, y);

        if (!(o1pg.x >= o2ld.x && o2pg.x >= o1ld.x && o1pg.y >= o2ld.y && o2pg.y >= o1ld.y)) {
            return 0;
        }

        MyPoint vP3P1 = new MyPoint(o2.P1.x - o1.P1.x, o2.P1.y - o1.P1.y);
        MyPoint vP2P1 = new MyPoint(o1.P2.x - o1.P1.x, o1.P2.y - o1.P1.y);
        MyPoint vP4P1 = new MyPoint(o2.P2.x - o1.P1.x, o2.P2.y - o1.P1.y);

        double det1 = det(vP3P1, vP2P1);
        double det2 = det(vP4P1, vP2P1);

        MyPoint vP1P3 = new MyPoint(o1.P1.x - o2.P1.x, o1.P1.y - o2.P1.y);
        MyPoint vP4P3 = new MyPoint(o2.P2.x - o2.P1.x, o2.P2.y - o2.P1.y);
        MyPoint vP2P3 = new MyPoint(o1.P2.x - o2.P1.x, o1.P2.y - o2.P1.y);

        double det3 = det(vP1P3, vP4P3);
        double det4 = det(vP2P3, vP4P3);

        if (det1 * det2 > 0 || det3 * det4 > 0) {
            return 0;
        }
        if (Math.abs(det1) < 1e-8 && Math.abs(det2) < 1e-8) {
            return 2;
        } else {
            if (o1.getP1().x == o1.getP2().x) {
                x = o1.getP1().x;
                y = (o2.getP2().y - o2.getP1().y) * x / (o2.getP2().x - o2.getP1().x) + o2.getP1().y;
            } else if (o2.getP1().x == o2.getP2().x) {
                x = o2.getP1().x;
                y = (o1.getP2().y - o1.getP1().y) * x / (o1.getP2().x - o1.getP1().x) + o1.getP1().y;
            } else {

                double a1 = (o1.getP2().y - o1.getP1().y) / (o1.getP2().x - o1.getP1().x);
                double a2 = (o2.getP2().y - o2.getP1().y) / (o2.getP2().x - o2.getP1().x);
                double b1 = o1.getP1().y - o1.getP1().x * (o1.getP2().y - o1.getP1().y) / (o1.getP2().x - o1.getP1().x);
                double b2 = o2.getP1().y - o2.getP1().x * (o2.getP2().y - o2.getP1().y) / (o2.getP2().x - o2.getP1().x);
                x = ((b2 - b1) / (a1 - a2));
                y = (a1 * (b2 - b1) / (a1 - a2) + b1);
            }
            p.x = x;
            p.y = y;
            return 1;
        }
    }

    public static double det(MyPoint p1, MyPoint p2) {
        return p1.x * p2.y - p1.y * p2.x;
    }

    public Rectangle ograniczenieOdcinka(Point p, Point k) {
        Rectangle wynik;
        int x1 = (int) p.getX();
        int x2 = (int) k.getX();
        int y1 = (int) p.getY();
        int y2 = (int) k.getY();

        if (p.x < k.x) {
            if (p.y > k.y) {
                wynik = new Rectangle(x1, y1, x2 - x1, y1 - y2);
            } else {
                wynik = new Rectangle(x1, y1 + (y2 - y1), x2 - x1, y2 - y1);
            }
        } else if (k.y > p.y) {
            wynik = new Rectangle(x2, y2, x1 - x2, y2 - y1);
        } else {
            wynik = new Rectangle(x2, y2 + (y1 - y2), x1 - x2, y1 - y2);
        }

        return wynik;
    }

    @Override
    public int compareTo(MyEdge e) {
        if (this == e || (P1.compareTo(e.P1) == 0 && P2.compareTo(e.P2) == 0)) {
            return 0;
        }

        double x1, x2;
        double yL = Pe.y;

        if (P1.y == P2.y) {
            x1 = P2.x;
        } else if (P1.x == P2.x) {
            x1 = P1.x;
        } else {
            x1 = (yL - P1.y) * (P2.x - P1.x) / (P2.y - P1.y) + P1.x;
        }

        if (e.P1.y == e.P2.y) {
            x2 = e.P2.x;
        } else if (e.P1.x == e.P2.x) {
            x2 = e.P1.x;
        } else {
            x2 = (yL - e.P1.y) * (e.P2.x - e.P1.x) / (e.P2.y - e.P1.y) + e.P1.x;
        }

        if (Math.abs(x1 - x2) < 1e-10) {
            if (Math.abs(x1 - Pe.x) < 1e-10 || x1 < Pe.x) {
                x1 = ((yL - 1) - P1.y) * (P2.x - P1.x) / (P2.y - P1.y) + P1.x;
                x2 = ((yL - 1) - e.P1.y) * (e.P2.x - e.P1.x) / (e.P2.y - e.P1.y) + e.P1.x;
            } else {
                x1 = ((yL + 1) - P1.y) * (P2.x - P1.x) / (P2.y - P1.y) + P1.x;
                x2 = ((yL + 1) - e.P1.y) * (e.P2.x - e.P1.x) / (e.P2.y - e.P1.y) + e.P1.x;
            }
        }

        return x1 < x2 ? -1 : 1;

    }

    public double distance(MyPoint P) {
        double A = P2.y - P1.y;
        double B = P2.x - P1.x;
        double distValue = Math.abs(-A * P.x + B * P.y - B * P1.y + A * P1.x) / Math.sqrt(A * A + B * B);
        return distValue;
    }

}

class EdgeComparator implements Comparator<MyEdge> {

    @Override
    public int compare(MyEdge o1, MyEdge o2) {
        if (o1 == o2) {
            return 0;
        }

        int comp = o1.getP1().compareTo(o2.P1);

        if (comp == 0) {
            return o1.getP2().compareTo(o2.getP2());
        }
        return comp;
    }

}
