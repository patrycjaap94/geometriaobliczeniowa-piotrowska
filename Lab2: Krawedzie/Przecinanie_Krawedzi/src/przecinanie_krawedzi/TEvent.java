/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package przecinanie_krawedzi;

import java.util.Comparator;
import java.util.TreeSet;

/**
 *
 * @author patrycja
 */
public class TEvent implements Comparable<TEvent> {

    private MyPoint p;
    private TreeSet<MyEdge> startPoints;
    private TreeSet<MyEdge> endPoints;
    private TreeSet<MyEdge> crossPoints;

    public TEvent(MyPoint p) {
        this.p = new MyPoint(p);
        startPoints = new TreeSet<>();
        endPoints = new TreeSet<>();
        crossPoints = new TreeSet<>();
    }
    public MyPoint getP()

    {
        return p;
    }

    @Override
    public int compareTo(TEvent e) {
        if (p.x == e.p.x && p.y == e.p.y) {
            return 0;
        }
        if (p.y == e.p.y) {
            return p.x < e.p.x ? -1 : 1;
        } else {
            return p.y > e.p.y ? -1 : 1;
        }

    }

    public void addStartPoint(MyEdge e) {
        startPoints.add(e);

    }

    public void addEndPoint(MyEdge e) {
        endPoints.add(e);

    }

    public void addCrossPoint(MyEdge e) {
        crossPoints.add(e);

    }

    public TreeSet<MyEdge> getStartPoints() {
        return startPoints;
    }

    public TreeSet<MyEdge> getEndPoints() {
        return endPoints;
    }

    public TreeSet<MyEdge> getCrossPoints() {
        return crossPoints;
    }
}
