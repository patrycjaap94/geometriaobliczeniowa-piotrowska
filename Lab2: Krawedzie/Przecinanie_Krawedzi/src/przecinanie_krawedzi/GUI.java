/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package przecinanie_krawedzi;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.TreeSet;

/**
 *
 * @author justyna
 */
public class GUI extends javax.swing.JFrame {

    /**
     * Creates new form GUI
     */
    TreeSet<MyEdge> ol;
    private LinkedList<MyEdge> edges = new LinkedList<>();  // lista punkow

    public GUI() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        ekran = new DrawPanel();
        wyczysc = new javax.swing.JButton();
        uruchom = new javax.swing.JButton();
        corss = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout ekranLayout = new javax.swing.GroupLayout(ekran);
        ekran.setLayout(ekranLayout);
        ekranLayout.setHorizontalGroup(
            ekranLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        ekranLayout.setVerticalGroup(
            ekranLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 234, Short.MAX_VALUE)
        );

        wyczysc.setText("wyczysc");
        wyczysc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wyczyscActionPerformed(evt);
            }
        });

        uruchom.setText("uruchom");
        uruchom.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uruchomActionPerformed(evt);
            }
        });

        corss.setText("jButton1");
        corss.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                corssActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ekran, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(uruchom)
                .addGap(28, 28, 28)
                .addComponent(corss)
                .addGap(29, 29, 29)
                .addComponent(wyczysc)
                .addContainerGap(35, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addComponent(ekran, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(uruchom)
                    .addComponent(corss)
                    .addComponent(wyczysc))
                .addGap(25, 25, 25))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void wyczyscActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_wyczyscActionPerformed
        // TODO add your handling code here:
        ol.clear();
        ((DrawPanel) (ekran)).clear();
        edges.clear();
    }//GEN-LAST:event_wyczyscActionPerformed

    private void uruchomActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uruchomActionPerformed
        // TODO add your handling code here:
        ol = new TreeSet<>(new EdgeComparator());
        Random rand = new Random();
        int w = ekran.getWidth() - 5;
        int h = ekran.getHeight() - 5;

        MyEdge p = null;
        wyczyscActionPerformed(null);
        while (ol.size() < 10) {
            p = new MyEdge(rand.nextInt(w), rand.nextInt(h), rand.nextInt(w), rand.nextInt(h));
            ol.add(p);
        }
        for (MyEdge pp : ol) {
            ((DrawPanel) (ekran)).drawPoint(pp.P1);
            ((DrawPanel) (ekran)).drawPoint(pp.P2);
            ((DrawPanel) (ekran)).drawLine(pp);
        }
        
        edges.addAll(ol);  // wstawiamy na listę nowo wygenerowane punkty
    }//GEN-LAST:event_uruchomActionPerformed

    private void corssActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_corssActionPerformed
        // TODO add your handling code here:
        TreeSet<MyPoint> crossPoints = edgeCrossing(edges);

        for (MyPoint p : crossPoints) {
            ((DrawPanel) (ekran)).drawPoint(p);

        }
    }//GEN-LAST:event_corssActionPerformed

    public TreeSet<MyPoint> edgeCrossing(LinkedList<MyEdge> edgeList) {
        TreeSet<MyPoint> crossPoints = new TreeSet<>();
        TEventQueue queue = new TEventQueue();

        for (MyEdge edge : edgeList) {
            TEvent e = new TEvent(edge.getP1());
            e.addStartPoint(edge);
            queue.addEvent(e);
            TEvent e1 = new TEvent(edge.getP2());
            e1.addEndPoint(edge);
            queue.addEvent(e1);
        }
        TState state = new TState();
        while (queue.size() > 0) {
            TEvent e = queue.getEvent();
            TreeSet<MyEdge> upper = e.getStartPoints();
            TreeSet<MyEdge> lower = e.getEndPoints();
            TreeSet<MyEdge> cross = e.getCrossPoints();
            if (upper.size() + lower.size() + cross.size() > 1) {
                crossPoints.add(e.getP());
            }
            TreeSet<MyEdge> T = new TreeSet<>(state);
            state.clear();
            MyPoint p = e.getP();
            MyEdge.set_pE(p);
            for (MyEdge edge : T) {
                state.add(edge);
            }

            for (MyEdge edge : upper) {
                state.add(edge);
            }

            for (MyEdge edge : cross) {
                state.add(edge);
            }
            for (MyEdge edge : lower) {
                state.remove(edge);
            }
            if (upper.size() + cross.size() == 0) {
                MyPoint p1 = new MyPoint(p.x, p.y + 1);
                MyPoint p2 = new MyPoint(p.x, p.y - 1);
                MyEdge ep = new MyEdge(p1, p2, Color.BLACK);
                state.add(ep);
                MyEdge left = state.onLeft(ep);
                MyEdge right = state.onRight(ep);
                state.remove(ep);

                MyPoint p_0 = new MyPoint(0, 0),
                        p_1 = new MyPoint(0, 0);
                if (left != null && right != null) {
                    int cross_points = MyEdge.EdgeCross(left, right, p_0, p_1);
                                                System.out.print(cross_points);
                    if (cross_points == 2) { // krawędzie częściowo lub w całości pokrywają się - jeszcze nie ma obsługi
                    } else if (cross_points == 1) // jeden punkt przecięcia, wynik jest w p_0
                    {
                        if ((p_0.y < e.getP().y) || (p_0.y == e.getP().y && p_0.x > e.getP().x)) { // punkt przecięcia poniżej lub na prawo od punktu zdarzeń
                            TEvent new_event = new TEvent(p_0);
                            new_event.addCrossPoint(left);
                            new_event.addCrossPoint(right);
                            queue.addEvent(new_event);
                        }
                    }
                }
            } else // jest co najmniej 1 odcinek w punkcie zdarzeń
            {     // doszły nowe krawędzie, które mają początek w p lub zmieniła się kolejność krzyżujących się krawędzi - sprawdzamy przecięcia
                // wyznaczamy skrajnie lewą krawędź - szukamy przecięcia z jej lewym sąsiadem
                // wyznaczamy skrajnie prawą krawędź - szukamy przecięcia z jej prawym sąsiadem
                TState temp = new TState(); // tylko krawędzie mające górny koniec lub przecinające się w aktualnym punktem zdarzeń
                for (MyEdge edge : upper) {
                    temp.add(edge);
                }
                for (MyEdge edge : lower) {
                    cross.remove(edge);
                }
                for (MyEdge edge : cross) {
                    temp.add(edge);
                }
                MyEdge l = temp.first();
                MyEdge r = temp.last();
                MyPoint p_0 = new MyPoint(0, 0),
                        p_1 = new MyPoint(0, 0);
                MyEdge left = state.onLeft(l);  // lewy sąsiad
                MyEdge right = state.onRight(r); // prawy sąsiad
                if (left != null) {
                    int cross_points = MyEdge.EdgeCross(left, l, p_0, p_1);
                    if (cross_points == 2) { // krawędzie częściowo lub w całości pokrywają się - jeszcze nie ma obsługi
                    } else if (cross_points == 1) // jeden punkt przecięcia, wynik jest w p_0
                    {
                        if ((p_0.y < e.getP().y) || (p_0.y == e.getP().y && p_0.x > e.getP().x)) { // punkt przecięcia poniżej lub na prawo od punktu zdarzeń
                            try {
                                TEvent new_event;
                                new_event = new TEvent((MyPoint) p_0.clone());
                                new_event.addCrossPoint(left);
                                new_event.addCrossPoint(l);
                                queue.addEvent(new_event);
                            } catch (CloneNotSupportedException ex) {

                            }
                        }
                    }
                }
                if (right != null) {
                    int cross_points = MyEdge.EdgeCross(right, r, p_0, p_1);
                    if (cross_points == 2) { // krawędzie częściowo lub w całości pokrywają się - jeszcze nie ma obsługi
                    } else if (cross_points == 1) // jeden punkt przecięcia, wynik jest w p_0
                    {
                        if ((p_0.y < e.getP().y) || (p_0.y == e.getP().y && p_0.x > e.getP().x)) { // punkt przecięcia poniżej lub na prawo od punktu zdarzeń
                            try {
                                TEvent new_event = new TEvent((MyPoint) p_0.clone());
                                new_event.addCrossPoint(r);
                                new_event.addCrossPoint(right);
                                queue.addEvent(new_event);
                            } catch (CloneNotSupportedException ex) {

                            }
                        }
                    }
                }
            }
        }
        return crossPoints;
    }

    /**
     * @param args the command line arguments
     */
    public static void main() {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton corss;
    private javax.swing.JPanel ekran;
    private javax.swing.JButton uruchom;
    private javax.swing.JButton wyczysc;
    // End of variables declaration//GEN-END:variables
}
