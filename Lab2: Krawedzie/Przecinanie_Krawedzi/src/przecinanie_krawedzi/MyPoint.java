/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package przecinanie_krawedzi;

import java.awt.Point;
import java.util.Comparator;

public class MyPoint implements Comparable<MyPoint> {

    double x, y;

    public MyPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }

    MyPoint(MyPoint p) {
        x = p.x;
        y = p.y;
    }

    public String toString() {
        return x + ", " + y;
    }

    public int compareTo(MyPoint p) {
        if (x == p.x && y == p.y) {
            return 0;
        }
        if (x == p.x) {
            return y < p.y ? -1 : 1;
        } else {
            return x < p.x ? -1 : 1;
        }
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
