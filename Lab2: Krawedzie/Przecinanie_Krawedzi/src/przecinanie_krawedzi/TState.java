/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package przecinanie_krawedzi;

import java.util.Iterator;
import java.util.TreeSet;

/**
 *
 * @author patrycja
 */
public class TState extends TreeSet<MyEdge> {

    @Override
    public MyEdge first() {
        if (size() > 0) {
            return super.first();
        }
        return null;

    }

    @Override
    public MyEdge last() {
        if (size() > 0) {
            return super.last();
        }
        return null;

    }

    @Override
    public boolean add(MyEdge e) {
        double yL = MyEdge.get_pE().y;
        if (yL <= e.P1.y && yL >= e.P2.y) {
            return super.add(e);
        } else {

            return false;
        }
    }

    public MyEdge onLeft(MyEdge e) {

        Iterator<MyEdge> it = iterator();
        MyEdge left = null;
        if (it.hasNext()) {
            left = it.next();
        } else {
            return null;
        }
        MyEdge edge;
        while (it.hasNext()) {
            edge = it.next();
            if (edge == e) {
                return left;
            } else {
                left = edge;
            }
        }
        return null;

    }

    public MyEdge onRight(MyEdge e) {

        Iterator<MyEdge> it = iterator();

        MyEdge edge;
        while (it.hasNext()) {
            edge = it.next();
            if (edge == e) {
                if (it.hasNext()) {
                    return it.next();
                }
            } else {
                return null;
            }
        }
        return null;

    }
}
